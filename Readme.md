# Achievement:  
### CreateDTO  
private TypeOfAchievement typeOfAchievement;->(not null)  
private String img;  
### FullDTO  
private Integer id;->(not null)  
private TypeOfAchievement typeOfAchievement;->(not null)  
private String img;  
private List&lt;PlayerPreviewDto> players;  
### PreviewDTO  
private Integer id;->(not null)  
private TypeOfAchievement typeOfAchievement;->(not null)  
private String img;  
### UpdateDTO  
private Integer id;->(not null)  
private TypeOfAchievement typeOfAchievement;->(not null)  
private String img;  
***

# Player:  
### CreateDTO  
private Integer userId;->(not null)  
### FullDTO  
private Integer id;->(not null)  
private Integer userId;->(not null)   
private PlayerRank playerRank;->(not null)  
private List&lt;PlayerProgressFullDto> playersProgress;  
private List&lt;AchievementFullDto> achievements;  
### PreviewDTO  
private Integer userId;->(not null)  
private PlayerRank playerRank;->(not null)  
private List&lt;AchievementFullDto> achievements;  
***

# PlayerProgress  
### CreateDTO  
private PlayerActionOfAchievement action;  
private Integer playerId;  
### FullDTO  
private Integer id;->(not null)   
private PlayerActionOfAchievement action;  
private Integer counterAction;->(not null)  
private Integer playerId;  