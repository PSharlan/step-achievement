package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;

import java.util.List;

public interface PlayerService {

    PlayerFullDto findById(Integer id);

    List<PlayerPreviewDto> findAll();

    PlayerFullDto create(PlayerCreateDto createDto);

    void delete(Integer id);
}
