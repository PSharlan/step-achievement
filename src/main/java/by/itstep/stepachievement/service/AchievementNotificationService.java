package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;

import java.util.List;

public interface AchievementNotificationService {

    List<AchievementNotificationFullDto> findAllByUserId(Integer userId);

    void clearById(Integer notificationId);

    void clearByUserId(Integer userId);
}
