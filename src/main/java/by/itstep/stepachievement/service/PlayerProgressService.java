package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;

import java.util.List;

public interface PlayerProgressService {

    PlayerProgressFullDto findById(Integer id);

    List<PlayerProgressFullDto> findAll();

    PlayerProgressFullDto processAction(PlayerCommittedActionDto committedAction);

    void delete(Integer id);
}
