package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementUpdateDto;

import java.util.List;

public interface GroupAchievementService {

    GroupAchievementFullDto findById(Integer id);

    List<GroupAchievementFullDto> findAll();

    GroupAchievementFullDto create(GroupAchievementCreateDto createDto);

    GroupAchievementFullDto update(GroupAchievementUpdateDto updateDto);

    void delete(Integer id);
}
