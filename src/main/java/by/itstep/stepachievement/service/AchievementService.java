package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.dto.achievement.AchievementUpdateDto;
import by.itstep.stepachievement.entity.enums.Action;

import java.util.List;

public interface AchievementService {

    AchievementFullDto findById(Integer id);

    List<AchievementPreviewDto> findAll();

    List<Action> findAllAction();

    AchievementFullDto create(AchievementCreateDto createDto);

    AchievementFullDto update(AchievementUpdateDto updateDto);

    void delete(Integer id);
}
