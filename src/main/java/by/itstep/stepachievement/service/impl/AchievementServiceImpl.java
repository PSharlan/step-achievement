package by.itstep.stepachievement.service.impl;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.dto.achievement.AchievementUpdateDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.enums.Action;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.exception.InvalidEntityException;
import by.itstep.stepachievement.mapper.AchievementMapper;
import by.itstep.stepachievement.repository.AchievementRepository;
import by.itstep.stepachievement.repository.GroupAchievementRepository;
import by.itstep.stepachievement.service.AchievementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class AchievementServiceImpl implements AchievementService {

    @Autowired
    private AchievementRepository repository;

    @Autowired
    private AchievementMapper mapper;

    @Autowired
    private GroupAchievementRepository groupAchievementRepository;

    @Override
    @Transactional(readOnly = true)
    public AchievementFullDto findById(Integer id) {
        AchievementFullDto foundEntity = repository.findById(id)
                .map(mapper::mapToFullDto)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "Achievement was not found by id: %s ", id))
                );

        log.info("AchievementServiceImpl -> found achievement: {} by id: {}", foundEntity, id);
        return foundEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AchievementPreviewDto> findAll() {
        List<AchievementEntity> foundEntities = repository.findAll();

        log.info("AchievementServiceImpl -> found {} achievements", foundEntities.size());
        return mapper.mapToPreviewDtoList(foundEntities);
    }

    @Override
    public List<Action> findAllAction(){
        return Arrays.asList(Action.values());
    }

    @Override
    @Transactional
    public AchievementFullDto create(AchievementCreateDto createDto) {
        AchievementEntity entityToSave = mapper.mapToEntity(createDto);

        GroupAchievementEntity groupAchievementEntity = checkGroupAchievement(createDto.getGroupId());
        entityToSave.setGroupAchievement(groupAchievementEntity);

        checkAchievementToSave(entityToSave);

        AchievementEntity savedEntity = repository.save(entityToSave);

        log.info("AchievementServiceImpl -> created {} successfully", savedEntity);
        return mapper.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public AchievementFullDto update(AchievementUpdateDto updateDto) {
        AchievementEntity entityUpdate = repository.findById(updateDto.getId())
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "Achievement was not found by id: %s ", updateDto.getId()))
                );

        GroupAchievementEntity groupAchievementEntity = checkGroupAchievement(updateDto.getGroupId());

        entityUpdate.setGroupAchievement(groupAchievementEntity);
        entityUpdate.setImg(updateDto.getImg());
        entityUpdate.setAction(updateDto.getAction());
        entityUpdate.setThreshold(updateDto.getThreshold());
        entityUpdate.setLevel(updateDto.getLevel());
        entityUpdate.setTypeOfAchievement(updateDto.getTypeOfAchievement());

        checkAchievementToSave(entityUpdate);

        AchievementEntity updatedEntity = repository.save(entityUpdate);

        log.info("AchievementServiceImpl -> achievement {} was successfully updated ", updatedEntity);
        return mapper.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        if (!repository.existsById(id)){
            throw new EntityIsNotFoundException(String.format("Achievement was not found by id %s: ", id));
        }
        repository.deleteById(id);

        log.info("AchievementServiceImpl -> achievement by id: {} successfully deleted", id);
    }

    private void checkAchievementToSave(AchievementEntity achievementEntity){
        String type = achievementEntity.getTypeOfAchievement();
        AchievementEntity foundAchievementEntity =
                repository.findOneByTypeOfAchievement(type);
        if (foundAchievementEntity != null && !foundAchievementEntity.getId().equals(achievementEntity.getId())){
                throw new InvalidEntityException(String.format("Achievement with type %s already exists", type));
        }
    }

    private GroupAchievementEntity checkGroupAchievement(Integer groupId){
        return groupAchievementRepository.findById(groupId)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "GroupAchievement was not found by id: %s ", groupId))
                );
    }
}
