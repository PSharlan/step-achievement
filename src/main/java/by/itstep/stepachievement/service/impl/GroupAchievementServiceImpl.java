package by.itstep.stepachievement.service.impl;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementUpdateDto;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.exception.InvalidEntityException;
import by.itstep.stepachievement.mapper.GroupAchievementMapper;
import by.itstep.stepachievement.repository.GroupAchievementRepository;
import by.itstep.stepachievement.service.GroupAchievementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class GroupAchievementServiceImpl implements GroupAchievementService {

    @Autowired
    private GroupAchievementRepository repository;

    @Autowired
    private GroupAchievementMapper mapper;

    @Override
    @Transactional(readOnly = true)
    public GroupAchievementFullDto findById(Integer id) {
        GroupAchievementFullDto foundEntity = repository.findById(id)
                .map(mapper::mapToFullDto)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "GroupAchievement was not found by id: %s ", id))
                );

        log.info("GroupAchievementServiceImpl -> found achievement: {} by id: {}", foundEntity, id);
        return foundEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public List<GroupAchievementFullDto> findAll() {
        List<GroupAchievementEntity> foundEntities = repository.findAll();

        log.info("GroupAchievementServiceImpl -> found {} groupAchievements", foundEntities.size());
        return mapper.mapToFullDtoList(foundEntities);
    }

    @Override
    @Transactional
    public GroupAchievementFullDto create(GroupAchievementCreateDto createDto) {
        GroupAchievementEntity entityToSave = mapper.mapToEntity(createDto);

        if (repository.findByGroupName(createDto.getGroupName()) != null){
            throw new InvalidEntityException(String.format(
                    "GroupAchievement with group name %s already exists", createDto.getGroupName()));
        }

        GroupAchievementEntity savedEntity = repository.save(entityToSave);

        log.info("GroupAchievementServiceImpl -> created {} successfully", savedEntity);
        return mapper.mapToFullDto(savedEntity);
    }
    
    @Override
    @Transactional
    public GroupAchievementFullDto update(GroupAchievementUpdateDto updateDto) {
        GroupAchievementEntity entityUpdate = repository.findById(updateDto.getId())
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "GroupAchievement was not found by id: %s", updateDto.getId()))
                );

        GroupAchievementEntity foundGroupName =
                repository.findByGroupName(updateDto.getGroupName());
        if (foundGroupName != null && !foundGroupName.getId().equals(entityUpdate.getId())){
            throw new InvalidEntityException(String.format(
                    "GroupAchievement with group name %s already exists", entityUpdate.getGroupName()));
        }

        entityUpdate.setGroupName(updateDto.getGroupName());
        entityUpdate.setBackgroundImg(updateDto.getBackgroundImg());

        GroupAchievementEntity updatedEntity = repository.save(entityUpdate);

        log.info("GroupAchievementServiceImpl -> group achievement {} was successfully updated ", updatedEntity);
        return mapper.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        if (!repository.existsById(id)){
            throw new EntityIsNotFoundException(String.format("GroupAchievement was not found by id: %s ", id));
        }
        repository.deleteById(id);

        log.info("GroupAchievementServiceImpl -> group achievement by id: {} successfully deleted", id);
    }
}
