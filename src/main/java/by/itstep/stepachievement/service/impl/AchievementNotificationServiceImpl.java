package by.itstep.stepachievement.service.impl;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;
import by.itstep.stepachievement.entity.AchievementNotificationEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.mapper.AchievementNotificationMapper;
import by.itstep.stepachievement.repository.AchievementNotificationRepository;
import by.itstep.stepachievement.repository.PlayerRepository;
import by.itstep.stepachievement.service.AchievementNotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AchievementNotificationServiceImpl implements AchievementNotificationService {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private AchievementNotificationRepository achievementNotificationRepository;

    @Autowired
    private AchievementNotificationMapper mapper;

    @Override
    @Transactional(readOnly = true)
    public List<AchievementNotificationFullDto> findAllByUserId(Integer userId) {
        PlayerEntity playerEntity = playerRepository.findByUserId(userId);

        if (playerEntity == null) {
            throw new EntityIsNotFoundException(String.format("User was not found by id: %s ", userId));
        }

         List<AchievementNotificationEntity> foundNotification =
                achievementNotificationRepository.findAllByPlayerId(playerEntity.getId());

        return foundNotification.stream()
                .map(notification -> mapper.mapToFullDto(notification))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void clearById(Integer notificationId) {
        if (!achievementNotificationRepository.existsById(notificationId)){
            throw new EntityIsNotFoundException(String.format(
                    "Achievement notification was not found by id %s: ", notificationId));
        }
        achievementNotificationRepository.deleteById(notificationId);

        log.info("AchievementNotificationServiceImpl -> achievement notification by id: {} successfully deleted"
                , notificationId);
    }

    @Override
    @Transactional
    public void clearByUserId(Integer userId) {
       List<AchievementNotificationFullDto> achievementNotificationFullDtos = findAllByUserId(userId);

       if(!achievementNotificationFullDtos.isEmpty())

       for (AchievementNotificationFullDto achievementNotificationFullDto : achievementNotificationFullDtos){
           achievementNotificationRepository.deleteById(achievementNotificationFullDto.getId());
       }
    }
}
