package by.itstep.stepachievement.service.impl;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.exception.InvalidEntityException;
import by.itstep.stepachievement.mapper.PlayerMapper;
import by.itstep.stepachievement.repository.PlayerRepository;
import by.itstep.stepachievement.service.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static by.itstep.stepachievement.entity.enums.PlayerRank.JUNIOR;

@Slf4j
@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerRepository repository;

    @Autowired
    private PlayerMapper mapper;

    @Override
    @Transactional(readOnly = true)
    public PlayerFullDto findById(Integer id) {
        PlayerFullDto foundEntity = repository.findById(id)
                .map(mapper::mapToFullDto)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "Player was not found by id: %s", id))
                );

        log.info("PlayerServiceImpl -> found player: {} by id: {}", foundEntity, id);
        return foundEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlayerPreviewDto> findAll() {
        List<PlayerEntity> foundEntities = repository.findAll();

        log.info("PlayerServiceImpl -> found {} Players", foundEntities.size());
        return mapper.mapToPreviewDtoList(foundEntities);
    }

    @Override
    @Transactional
    public PlayerFullDto create(PlayerCreateDto createDto) {
        PlayerEntity toSave = mapper.mapToEntity(createDto);
        toSave.setPlayerRank(JUNIOR);

        checkPlayerToSave(toSave);

        PlayerEntity savedEntity = repository.save(toSave);

        log.info("PlayerServiceImpl -> created  {} successfully", savedEntity);
        return mapper.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        if (!repository.existsById(id)){
            throw new EntityIsNotFoundException(String.format("Player was not found by id: %s ", id));
        }
        repository.deleteById(id);

        log.info("PlayerServiceImpl -> player by id: {} successfully deleted", id);
    }

    private void checkPlayerToSave(PlayerEntity playerEntity){
       PlayerEntity foundPlayer = repository.findByUserId(playerEntity.getUserId());

       if (foundPlayer != null){
           throw new InvalidEntityException(String.format
                       ("Player was not saved by id: %s ", foundPlayer.getUserId()));
       }
    }
}
