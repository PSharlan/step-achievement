package by.itstep.stepachievement.service.impl;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.AchievementNotificationEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.entity.PlayerProgressEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.mapper.PlayerProgressMapper;
import by.itstep.stepachievement.repository.AchievementNotificationRepository;
import by.itstep.stepachievement.repository.AchievementRepository;
import by.itstep.stepachievement.repository.PlayerProgressRepository;
import by.itstep.stepachievement.repository.PlayerRepository;
import by.itstep.stepachievement.service.PlayerProgressService;
import by.itstep.stepachievement.service.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static by.itstep.stepachievement.entity.enums.Action.FINISH_GROUP;

@Slf4j
@Service
public class PlayerProgressServiceImpl implements PlayerProgressService {

    private static final Integer PLUS_ONE_COUNTER = 1;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private AchievementRepository achievementRepository;

    @Autowired
    private PlayerProgressRepository playerProgressRepository;

    @Autowired
    private PlayerProgressMapper playerProgressMapper;

    @Autowired
    private AchievementNotificationRepository achievementNotificationRepository;

    @Override
    @Transactional(readOnly = true)
    public PlayerProgressFullDto findById(Integer id) {
        PlayerProgressFullDto foundEntity = playerProgressRepository.findById(id)
                .map(playerProgressMapper::mapToFullDto)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "PlayerProgress was not found by id: %s", id))
                );

        log.info("PlayerProgressServiceImpl -> found playerProgress: {} by id: {}", foundEntity, id);
        return foundEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlayerProgressFullDto> findAll() {
        List<PlayerProgressEntity> foundEntities = playerProgressRepository.findAll();

        log.info("PlayerProgressServiceImpl -> found {} PlayerProgress", foundEntities.size());
        return playerProgressMapper.mapToPreviewDtoList(foundEntities);
    }

    private PlayerProgressFullDto create(PlayerProgressEntity entity) {
        PlayerProgressEntity savedEntity = playerProgressRepository.save(entity);

        log.info("PlayerProgressImpl -> created  {} successfully", savedEntity);
        return playerProgressMapper.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        if (!playerProgressRepository.existsById(id)){
            throw new EntityIsNotFoundException(String.format("PlayerProgress was not found by id: %s ", id));
        }
        playerProgressRepository.deleteById(id);

        log.info("PlayerProgressServiceImpl -> playerProgress by id: {} successfully deleted", id);
    }

    @Override
    @Transactional
    public PlayerProgressFullDto processAction(PlayerCommittedActionDto committedAction) {
        PlayerEntity foundPlayerEntity = playerRepository.findByUserId(committedAction.getUserId());
        if (foundPlayerEntity != null){
            PlayerProgressEntity foundPlayerProgressEntity = playerProgressRepository
                    .findOneByPlayerIdAndAction(
                            foundPlayerEntity.getId(), committedAction.getAction()
                    );

            if (foundPlayerProgressEntity != null){
                foundPlayerProgressEntity.setCounterAction(foundPlayerProgressEntity.getCounterAction() + PLUS_ONE_COUNTER);
                checkToGetAchievement(foundPlayerProgressEntity);
                create(foundPlayerProgressEntity);
                return playerProgressMapper.mapToFullDto(foundPlayerProgressEntity);
            }

            PlayerProgressEntity playerProgressEntity = PlayerProgressEntity
                    .builder()
                    .action(committedAction.getAction())
                    .player(foundPlayerEntity)
                    .counterAction(PLUS_ONE_COUNTER)
                    .build();
            PlayerProgressEntity savePlayerProgressEntity = playerProgressRepository.save(playerProgressEntity);
            foundPlayerEntity.getPlayersProgress().addAll(Arrays.asList(savePlayerProgressEntity));
            checkToGetAchievement(playerProgressEntity);
            create(playerProgressEntity);
            return playerProgressMapper.mapToFullDto(playerProgressEntity);
        }

        PlayerCreateDto playerCreateDto = new PlayerCreateDto();
        playerCreateDto.setUserId(committedAction.getUserId());
        PlayerFullDto playerFullDto = playerService.create(playerCreateDto);

        PlayerEntity playerEntity = playerRepository.findById(playerFullDto.getId())
                .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                        "PlayerProgress was not found by id: %s ", playerFullDto.getId()))
                );

        PlayerProgressEntity novelPlayerProgressEntity = playerProgressMapper.mapToEntity(committedAction);
        novelPlayerProgressEntity.setCounterAction(PLUS_ONE_COUNTER);
        novelPlayerProgressEntity.setPlayer(playerEntity);

        checkToGetAchievement(novelPlayerProgressEntity);
        create(novelPlayerProgressEntity);

        return playerProgressMapper.mapToFullDto(novelPlayerProgressEntity);
    }

    private void checkToGetAchievement(PlayerProgressEntity entity){
        List<AchievementEntity> achievementToGive = achievementRepository
                .findAllByActionAndThreshold(entity.getAction(), entity.getCounterAction());

        if (achievementToGive.size() == 0){
            return;
        }

        PlayerEntity player = playerRepository.findByUserId(entity.getPlayer().getUserId());
        player.getAchievements().addAll(achievementToGive);
        playerRepository.save(player);

        List<AchievementEntity> playerAllAchievement = player.getAchievements();
        for (AchievementEntity achievement : achievementToGive){
            List<AchievementEntity> allByGroupAchievementId = achievementRepository
                    .findAllByGroupAchievementId(achievement.getGroupAchievement().getId());

            int quantityAchievementInGroup = allByGroupAchievementId.size() -1;
            long quantityAchievementInPlayer = playerAllAchievement.stream()
                    .filter(a -> a.getGroupAchievement().getId().equals(achievement.getGroupAchievement().getId()))
                    .count();

            saveAchievementNotification(achievement, player);

            if ( quantityAchievementInGroup  == quantityAchievementInPlayer ){
                AchievementEntity findFinishGroupAchievement = achievementRepository.findOneByGroupAchievementIdAndAction(
                        achievement.getGroupAchievement().getId(), FINISH_GROUP
                );
                player.getAchievements().add(findFinishGroupAchievement);
                playerRepository.save(player);

               saveAchievementNotification(findFinishGroupAchievement, player);
            }
        }
    }

    private void saveAchievementNotification  (AchievementEntity achievement, PlayerEntity player){
        AchievementNotificationEntity saveAchievementNotification = AchievementNotificationEntity.builder()
                .achievement(achievement)
                .player(player)
                .build();
        achievementNotificationRepository.save(saveAchievementNotification);
    }
}
