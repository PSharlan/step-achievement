package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.AchievementNotificationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AchievementNotificationRepository extends JpaRepository<AchievementNotificationEntity, Integer> {

    List<AchievementNotificationEntity> findAllByPlayerId(Integer playerId);
}
