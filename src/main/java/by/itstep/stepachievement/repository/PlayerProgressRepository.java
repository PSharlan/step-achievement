package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.PlayerProgressEntity;
import by.itstep.stepachievement.entity.enums.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerProgressRepository extends JpaRepository<PlayerProgressEntity, Integer> {

    PlayerProgressEntity findOneByPlayerIdAndAction(Integer id, Action action);
}
