package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity, Integer> {

    PlayerEntity findByUserId(Integer id);
}
