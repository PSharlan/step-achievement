package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.GroupAchievementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupAchievementRepository extends JpaRepository<GroupAchievementEntity, Integer> {

    GroupAchievementEntity findByGroupName(String groupName);
}
