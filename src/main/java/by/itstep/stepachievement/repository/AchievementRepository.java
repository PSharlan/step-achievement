package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.enums.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AchievementRepository extends JpaRepository<AchievementEntity, Integer> {

    List<AchievementEntity> findAllByActionAndThreshold(Action action, Integer threshold);

    AchievementEntity findOneByTypeOfAchievement(String typeOfAchievement);

    List<AchievementEntity> findAllByGroupAchievementId(Integer groupId);

    AchievementEntity findOneByGroupAchievementIdAndAction(Integer groupId, Action action);
}
