package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.entity.PlayerEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PlayerMapper {

    PlayerFullDto mapToFullDto(PlayerEntity entity);

    PlayerEntity mapToEntity(PlayerCreateDto createDto);

    PlayerPreviewDto mapToPreviewDto(PlayerEntity entity);

    List<PlayerPreviewDto> mapToPreviewDtoList(List<PlayerEntity> entities);
}
