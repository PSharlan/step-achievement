package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;

import by.itstep.stepachievement.entity.AchievementEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = PlayerMapper.class)
public interface AchievementMapper {

    @Mapping(target = "groupId", source = "entity.groupAchievement.id")
    AchievementFullDto mapToFullDto(AchievementEntity entity);

    AchievementEntity mapToEntity(AchievementCreateDto createDto);

    AchievementPreviewDto mapToPreviewDto(AchievementEntity entity);

    List<AchievementPreviewDto> mapToPreviewDtoList(List<AchievementEntity> entities);
}
