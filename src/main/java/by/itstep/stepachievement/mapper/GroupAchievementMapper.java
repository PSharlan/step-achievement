package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupAchievementMapper {

    GroupAchievementFullDto mapToFullDto(GroupAchievementEntity entity);

    GroupAchievementEntity mapToEntity(GroupAchievementCreateDto createDto);

    List<GroupAchievementFullDto> mapToFullDtoList(List<GroupAchievementEntity> entities);
}
