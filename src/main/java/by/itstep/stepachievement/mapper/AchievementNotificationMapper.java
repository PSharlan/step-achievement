package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.AchievementNotificationEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {PlayerEntity.class, AchievementEntity.class})
public interface AchievementNotificationMapper {

    @Mapping(target = "achievementPreviewDto", source = "entity.achievement")
    @Mapping(target = "playerPreviewDto", source = "entity.player")
    AchievementNotificationFullDto mapToFullDto (AchievementNotificationEntity entity);
}
