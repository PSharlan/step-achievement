package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.PlayerProgressEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PlayerProgressMapper {

    @Mapping(target = "playerId", source = "entity.player.userId")
    PlayerProgressFullDto mapToFullDto(PlayerProgressEntity entity);

    PlayerProgressEntity mapToEntity(PlayerCommittedActionDto createDto);

    List<PlayerProgressFullDto> mapToPreviewDtoList(List<PlayerProgressEntity> entities);
}
