package by.itstep.stepachievement.dto.achievementNotification;

import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import lombok.Data;

@Data
public class AchievementNotificationFullDto {

    private Integer id;
    private AchievementPreviewDto achievementPreviewDto;
    private PlayerPreviewDto playerPreviewDto;
}
