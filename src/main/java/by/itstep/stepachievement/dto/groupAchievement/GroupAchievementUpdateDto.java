package by.itstep.stepachievement.dto.groupAchievement;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GroupAchievementUpdateDto {

    @NotNull(message = "id can not be null")
    @ApiModelProperty(example = "1", notes = "Id of the current group of achievement")
    private Integer id;

    @NotNull(message = "Group name can not be null")
    @ApiModelProperty(name = "Group name", example = "Java road map")
    private String groupName;

    @NotNull(message = "Background img can not be null")
    @ApiModelProperty(name = "Background img")
    private String backgroundImg;
}
