package by.itstep.stepachievement.dto.groupAchievement;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GroupAchievementCreateDto {

    @NotNull(message = "Group name can not be null")
    @ApiModelProperty(name = "Group name", example = "Java road map")
    private String groupName;

    @NotNull(message = "Background img can not be null")
    @ApiModelProperty(name = "Background img")
    private String backgroundImg;
}
