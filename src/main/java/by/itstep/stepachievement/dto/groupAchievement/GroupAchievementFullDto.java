package by.itstep.stepachievement.dto.groupAchievement;

import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import lombok.Data;

import java.util.List;

@Data
public class GroupAchievementFullDto {

    private Integer id;
    private String groupName;
    private String backgroundImg;
    private List<AchievementPreviewDto> achievements;
}
