package by.itstep.stepachievement.dto.player;

import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.enums.PlayerRank;
import lombok.Data;

import java.util.List;

@Data
public class PlayerFullDto {

    private Integer id;
    private Integer userId;
    private PlayerRank playerRank;
    private List<PlayerProgressFullDto> playersProgress;
    private List<AchievementFullDto> achievements;
}
