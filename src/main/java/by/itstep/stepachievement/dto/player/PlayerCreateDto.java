package by.itstep.stepachievement.dto.player;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PlayerCreateDto {

    @ApiModelProperty(name = "user id", example = "15")
    @NotNull(message = "User id can not be null")
    private Integer userId;
}
