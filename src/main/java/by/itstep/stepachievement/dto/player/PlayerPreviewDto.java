package by.itstep.stepachievement.dto.player;

import by.itstep.stepachievement.entity.enums.PlayerRank;
import lombok.Data;

@Data
public class PlayerPreviewDto {

    private Integer userId;
    private PlayerRank playerRank;
}
