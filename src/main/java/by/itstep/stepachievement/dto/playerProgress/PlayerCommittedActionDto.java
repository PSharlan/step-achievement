package by.itstep.stepachievement.dto.playerProgress;

import by.itstep.stepachievement.entity.enums.Action;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PlayerCommittedActionDto {

    @NotNull(message = "Achievement action can not be empty")
    @ApiModelProperty(example = "TEST_CREATING", notes = "Action  have only values: TEST_CREATING or TEST_PASSING")
    private Action action;

    @NotNull(message = "User id can not be null")
    @ApiModelProperty(name = "user id", example = "15")
    private Integer userId;
}
