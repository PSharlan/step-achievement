package by.itstep.stepachievement.dto.playerProgress;

import by.itstep.stepachievement.entity.enums.Action;
import lombok.Data;

@Data
public class PlayerProgressFullDto {

    private Integer id;
    private Action action;
    private Integer counterAction;
    private Integer playerId;
}
