package by.itstep.stepachievement.dto.achievement;

import by.itstep.stepachievement.entity.enums.Action;
import lombok.Data;

@Data
public class AchievementPreviewDto {

    private Integer id;
    private String typeOfAchievement;
    private String img;
    private Integer threshold;
    private Action action;
    private Integer level;
}
