package by.itstep.stepachievement.dto.achievement;

import by.itstep.stepachievement.entity.enums.Action;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class AchievementUpdateDto {

    @NotNull(message = "id can not be null")
    @ApiModelProperty(example = "1", notes = "Id of the current achievement")
    private Integer id;

    @NotNull(message = "Achievement can not be null")
    @ApiModelProperty(example = "CREATE_FIVE_TEST")
    private String typeOfAchievement;

    @NotNull(message = "Img can not be null")
    @ApiModelProperty(example = "http://image.jpg")
    private String img;

    @NotNull(message = "Achievement action can not be empty")
    @ApiModelProperty(example = "TEST_CREATING", notes = "Action  have only values: TEST_CREATING or TEST_PASSING")
    private Action action;

    @Min(1)
    @NotNull(message = "Threshold can not be null")
    @ApiModelProperty(example = "5", notes = "Achievement threshold")
    private Integer threshold;

    @NotNull(message = "Achievement level can not be null")
    @ApiModelProperty(example = "3")
    private Integer level;

    @NotNull(message = "Group id can not be null")
    @ApiModelProperty(example = "2")
    private Integer groupId;
}
