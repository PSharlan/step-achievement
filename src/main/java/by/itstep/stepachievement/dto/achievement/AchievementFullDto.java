package by.itstep.stepachievement.dto.achievement;

import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.entity.enums.Action;
import lombok.Data;

import java.util.List;

@Data
public class AchievementFullDto {

    private Integer id;
    private String typeOfAchievement;
    private String img;
    private Integer threshold;
    private Action action;
    private List<PlayerPreviewDto> players;
    private Integer groupId;
    private Integer level;
}
