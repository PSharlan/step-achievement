package by.itstep.stepachievement.entity;

import by.itstep.stepachievement.entity.enums.Action;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "achievement")
public class AchievementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "type_of_achievement", nullable = false)
    private String typeOfAchievement;

    @Column(name = "img")
    private String img;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "action", nullable = false)
    private Action action;

    @Column(name = "threshold", nullable = false)
    private Integer threshold;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "achievement", fetch = FetchType.LAZY)
    private List<AchievementNotificationEntity> achievementNotifications = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "achievements", fetch = FetchType.LAZY)
    private List<PlayerEntity> players = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "group_achievement_id", nullable = false)
    private GroupAchievementEntity groupAchievement;

    @Column(name = "level", nullable = false)
    private Integer level;
}
