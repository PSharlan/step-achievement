package by.itstep.stepachievement.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "group_achievement")
public class GroupAchievementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "group_name", nullable = false, unique = true)
    private String groupName;

    @Column(name = "background_img", nullable = false)
    private String backgroundImg;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "groupAchievement", fetch = FetchType.LAZY)
    private List<AchievementEntity> achievements = new ArrayList<>();
}
