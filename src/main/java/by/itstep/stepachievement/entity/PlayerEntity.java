package by.itstep.stepachievement.entity;

import by.itstep.stepachievement.entity.enums.PlayerRank;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "player")
public class PlayerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "player_rank", nullable = false)
    private PlayerRank playerRank;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "player")
    private List<PlayerProgressEntity> playersProgress = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "player", fetch = FetchType.LAZY)
    private List<AchievementNotificationEntity> achievementNotifications = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "achievement_player",
            joinColumns = {@JoinColumn (name = "player_id")},
            inverseJoinColumns = {@JoinColumn (name = "achievement_id")}
    )
    private List<AchievementEntity> achievements = new ArrayList<>();
}
