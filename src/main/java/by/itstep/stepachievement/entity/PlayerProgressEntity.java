package by.itstep.stepachievement.entity;

import by.itstep.stepachievement.entity.enums.Action;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "player_progress")
public class PlayerProgressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "action")
    private Action action;

    @Column(name = "counter_action", nullable = false)
    private Integer counterAction;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "player_id")
    private PlayerEntity player;
}
