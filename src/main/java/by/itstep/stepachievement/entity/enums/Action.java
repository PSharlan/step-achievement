package by.itstep.stepachievement.entity.enums;

public enum Action {
    // step-test
    TEST_CREATING,
    TEST_PASSING,

    // step-roadmap
    CREATING_OF_THE_ROAD,
    BEGINNING_OF_THE_ROAD,
    MIDDLE_OF_THE_ROAD,
    END_OF_THE_ROAD,

    // step-feedback
    FOUND_BUG,

    // mentoring
    BECAME_A_MENTOR,
    BEGINNING_OF_THE_STUDYING_PROCESS,
    END_OF_THE_STUDYING_PROCESS,

    //main-achievement
    FINISH_GROUP
}
