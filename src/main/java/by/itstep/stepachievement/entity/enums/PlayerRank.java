package by.itstep.stepachievement.entity.enums;

public enum PlayerRank {

    JUNIOR,
    MIDDLE,
    SENOR
}
