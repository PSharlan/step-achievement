package by.itstep.stepachievement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(generationApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("by.itstep.stepachievement.controller"))
                .build();
    }

    private ApiInfo generationApiInfo(){
        return new ApiInfoBuilder()
                .title("Achievements api")
                .description("Api allows you to manage user achievements")
                .version("1.0")
                .build();
    }
}
