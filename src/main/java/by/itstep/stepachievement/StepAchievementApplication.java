package by.itstep.stepachievement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepAchievementApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepAchievementApplication.class, args);
	}

}
