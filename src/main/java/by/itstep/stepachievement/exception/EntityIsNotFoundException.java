package by.itstep.stepachievement.exception;

public class EntityIsNotFoundException extends RuntimeException{

    public EntityIsNotFoundException(String message){
        super(message);
    }
}
