package by.itstep.stepachievement.exception;

public class InvalidEntityException extends RuntimeException{

    public InvalidEntityException(String message){
        super(message);
    }
}
