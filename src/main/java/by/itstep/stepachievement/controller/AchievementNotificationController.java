package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;
import by.itstep.stepachievement.service.AchievementNotificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/achievementNotifications")
@Api(description = "The controller is designed to manage achievement notifications")
public class AchievementNotificationController {

    @Autowired
    private AchievementNotificationService achievementNotificationService;

    @GetMapping("/users/{userId}")
    @ApiOperation(value = "Find one achievement notification by user id")
    public List<AchievementNotificationFullDto> findById(@PathVariable Integer userId) {
        return achievementNotificationService.findAllByUserId(userId);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete achievement notifications by id")
    public void delete(@PathVariable Integer id){
        achievementNotificationService.clearById(id);
    }

    @DeleteMapping("/users/{userId}")
    @ApiOperation(value = "Delete all achievement notifications by user id")
    public void deleteAllNotificationByUserId(@PathVariable Integer userId){
        achievementNotificationService.clearByUserId(userId);
    }
}


