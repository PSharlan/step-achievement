package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.service.PlayerProgressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/playerProgress")
@Api(description = "The controller is designed to manage player progress")
public class PlayerProgressController {

    @Autowired
    private PlayerProgressService playerProgressService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one player progress by id", notes = "Existing id must be specified")
    public PlayerProgressFullDto findById(@PathVariable Integer id){
        return playerProgressService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all players progress")
    public List<PlayerProgressFullDto> findAll(){
        return playerProgressService.findAll();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete player progress by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id){
        playerProgressService.delete(id);
    }

    @PostMapping("/processAction")
    @ApiOperation(value = "Create player progress",
            notes = "Automatic creation of a player if he was not in the database")
    public PlayerProgressFullDto processAction(@Valid @RequestBody PlayerCommittedActionDto committedActionDto){
        return playerProgressService.processAction(committedActionDto);
    }
}
