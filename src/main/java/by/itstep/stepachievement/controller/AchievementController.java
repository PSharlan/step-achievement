package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.dto.achievement.AchievementUpdateDto;
import by.itstep.stepachievement.entity.enums.Action;
import by.itstep.stepachievement.service.AchievementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/achievements")
@Api(description = "The controller is designed to manage achievements")
public class AchievementController {

    @Autowired
    private AchievementService achievementService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one achievement by id", notes = "Existing id must be specified")
    public AchievementFullDto findById(@PathVariable Integer id){
        return achievementService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all achievements")
    public List<AchievementPreviewDto> findAll(){
        return achievementService.findAll();
    }

    @GetMapping("action")
    @ApiOperation(value = "Find all action")
    public List<Action>  findAllAction(){
        return achievementService.findAllAction();
    }

    @PostMapping("/create")
    @ApiOperation(value = "Create achievement")
    public AchievementFullDto create(@Valid @RequestBody AchievementCreateDto achievementCreateDto){
        return achievementService.create(achievementCreateDto);
    }

    @PutMapping("/update")
    @ApiOperation(value = "Update achievement")
    public AchievementFullDto update(@Valid @RequestBody AchievementUpdateDto achievementUpdateDto){
        return achievementService.update(achievementUpdateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete achievement by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id){
        achievementService.delete(id);
    }
}
