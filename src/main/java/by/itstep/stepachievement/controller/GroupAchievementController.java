package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementUpdateDto;
import by.itstep.stepachievement.service.GroupAchievementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/groupAchievements")
@Api(description = "The controller is designed to manage group of achievements")
public class GroupAchievementController {

    @Autowired
    private GroupAchievementService groupAchievementService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one group achievement by id", notes = "Existing id must be specified")
    public GroupAchievementFullDto findById(@PathVariable Integer id){
        return groupAchievementService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all group achievements")
    public List<GroupAchievementFullDto> findAll(){
        return groupAchievementService.findAll();
    }

    @PostMapping("/create")
    @ApiOperation(value = "Create group of achievement")
    public GroupAchievementFullDto create(@Valid @RequestBody GroupAchievementCreateDto groupAchievementCreateDto){
        return groupAchievementService.create(groupAchievementCreateDto);
    }

    @PutMapping("/update")
    @ApiOperation(value = "Update group of achievement")
    public GroupAchievementFullDto update(@Valid @RequestBody GroupAchievementUpdateDto groupAchievementUpdateDto){
        return groupAchievementService.update(groupAchievementUpdateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete group of achievement by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id){
        groupAchievementService.delete(id);
    }
}
