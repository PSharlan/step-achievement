package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.service.PlayerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/players")
@Api(description = "The controller is designed to manage players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one player by id", notes = "Existing id must be specified")
    public PlayerFullDto findById(@PathVariable Integer id){
        return playerService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all players")
    public List<PlayerPreviewDto> findAll(){
        return playerService.findAll();
    }

    @PostMapping("/create")
    @ApiOperation(value = "Create player")
    public PlayerFullDto create(@Valid @RequestBody PlayerCreateDto playerCreateDto){
        return playerService.create(playerCreateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete player by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id){
        playerService.delete(id);
    }
}
