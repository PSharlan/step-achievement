package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementUpdateDto;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.stepachievement.controller.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GroupAchievementControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        //given
        GroupAchievementEntity savedGroupAchievement = groupAchievementRepository.save(generationGroupAchievement());

        //when
        MvcResult result = mockMvc.perform(get("/groupAchievements/{id}", savedGroupAchievement.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupAchievementFullDto found = objectMapper.readValue(bytes, GroupAchievementFullDto.class);

        //then
        assertNotNull(found);
        assertNotNull(found.getId());
        assertEquals(savedGroupAchievement.getId(), found.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(get("/groupAchievements/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<GroupAchievementEntity> savedGroupAchievements = groupAchievementRepository
                .saveAll(generateListGroupAchievements());

        //when
        MvcResult result = mockMvc.perform(get("/groupAchievements" ))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<GroupAchievementFullDto> foundGroupAchievements = objectMapper
                .readValue(bytes, new TypeReference<List<GroupAchievementFullDto>>() {
                });

        //then
        assertNotNull(foundGroupAchievements);
        assertEquals(savedGroupAchievements.size(), foundGroupAchievements.size());
    }

    @Test
    void  create_happyPath() throws Exception {
        //given
        GroupAchievementCreateDto createDto = generateGroupAchievementCreateDto();

        MvcResult result = mockMvc.perform(post("/groupAchievements/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        //when
        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupAchievementFullDto savedGroupAchievement = objectMapper.readValue(bytes, GroupAchievementFullDto.class);

        //then
        assertNotNull(savedGroupAchievement);
    }

    @Test
    void create_exceptionPath() throws Exception {
        //given
        GroupAchievementCreateDto createDto = generateGroupAchievementCreateDto();

        createDto.setGroupName(null);

        //when
        mockMvc.perform(post("/groupAchievements/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
    }

    @Test
    void update_happyPath() throws Exception{
        //given
        GroupAchievementEntity savedGroupAchievement = groupAchievementRepository.save(generationGroupAchievement());

        GroupAchievementUpdateDto updateDto = generateGroupAchievementUpdateDto(
                savedGroupAchievement.getId(), "Hello Java", "Black");

        //when
        MvcResult result = mockMvc.perform(put("/groupAchievements/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupAchievementFullDto updateGroupAchievements = objectMapper.readValue(bytes, GroupAchievementFullDto.class);

        //then
        assertNotNull(updateGroupAchievements);
        assertNotNull(updateGroupAchievements.getId());
        assertEquals(savedGroupAchievement.getId(), updateGroupAchievements.getId());
        assertNotEquals(savedGroupAchievement.getGroupName(), updateGroupAchievements.getGroupName());
    }

    @Test
    void update_exceptionPath() throws Exception {
        //given
        GroupAchievementEntity savedGroupAchievement = groupAchievementRepository.save(generationGroupAchievement());

        GroupAchievementUpdateDto updateDto = generateGroupAchievementUpdateDto(
                savedGroupAchievement.getId(), "Hello Java", "Black");

        updateDto.setBackgroundImg(null);

        //when
        mockMvc.perform(put("/groupAchievements/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        GroupAchievementEntity savedGroupAchievement = groupAchievementRepository.save(generationGroupAchievement());

        //when
        MvcResult result = mockMvc.perform(delete("/groupAchievements/{id}", savedGroupAchievement.getId()))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(delete("/groupAchievements/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }
}
