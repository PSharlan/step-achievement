package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.PlayerProgressEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.stepachievement.controller.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlayerProgressControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        //given
        PlayerProgressEntity savedPlayerProgress = playerProgressRepository.save(generationPlayerProgress());

        //when
        MvcResult result = mockMvc.perform(get("/playerProgress/{id}", savedPlayerProgress.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlayerProgressFullDto found = objectMapper.readValue(bytes, PlayerProgressFullDto.class);

        //then
        assertNotNull(found);
        assertNotNull(found.getId());
        assertEquals(savedPlayerProgress.getId(), found.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(get("/playerProgress/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<PlayerProgressEntity> savedListPlayerProgress = playerProgressRepository.saveAll(generationListPlayerProgress());

        //when
        MvcResult result = mockMvc.perform(get("/playerProgress"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<PlayerProgressFullDto> foundPlayerProgress = objectMapper
                .readValue(bytes, new TypeReference<List<PlayerProgressFullDto>>() {
                });

        //then
        assertNotNull(foundPlayerProgress);
        assertEquals(savedListPlayerProgress.size(), foundPlayerProgress.size());
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        PlayerProgressEntity savedPlayerProgress = playerProgressRepository.save(generationPlayerProgress());

        //when
        MvcResult result = mockMvc.perform(delete("/playerProgress/{id}", savedPlayerProgress.getId()))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(delete("/playerProgress/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void processAction_happyPath() throws Exception {
        //given
        PlayerCommittedActionDto committedActionDto = generationPlayerCommittedActionDto();

        //when
        MvcResult result = mockMvc.perform(post("/playerProgress/processAction")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(committedActionDto)))
                .andExpect(status().isOk())
                .andReturn();

        //when
        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlayerProgressFullDto savedPlayerProgress = objectMapper.readValue(bytes, PlayerProgressFullDto.class);

        //then
        assertNotNull(savedPlayerProgress);
    }

    @Test
    void create_exceptionPaths() throws Exception {
        //given
        PlayerCommittedActionDto committedActionDto = generationPlayerCommittedActionDto();

        committedActionDto.setAction(null);

        //when
        mockMvc.perform(post("/playerProgress/processAction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(committedActionDto)))
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
    }
}
