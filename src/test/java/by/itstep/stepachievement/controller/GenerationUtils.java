package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementUpdateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementUpdateDto;
import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.entity.PlayerProgressEntity;

import java.util.Arrays;
import java.util.List;

import static by.itstep.stepachievement.entity.enums.Action.TEST_CREATING;
import static by.itstep.stepachievement.entity.enums.Action.TEST_PASSING;
import static by.itstep.stepachievement.entity.enums.PlayerRank.JUNIOR;

public class GenerationUtils {

    public static AchievementEntity generateAchievement(GroupAchievementEntity groupAchievementEntity){
        AchievementEntity entity = AchievementEntity.builder()
                .typeOfAchievement("CREATE_ONE_TEST")
                .action(TEST_CREATING)
                .groupAchievement(groupAchievementEntity)
                .threshold(5)
                .level(2)
                .img("http://img")
                .build();

        return entity;
    }

    public static List<AchievementEntity> generateListAchievement(GroupAchievementEntity groupAchievementEntity){
        AchievementEntity entity1 = AchievementEntity.builder()
                .typeOfAchievement("CREATE_ONE_TEST")
                .action(TEST_CREATING)
                .threshold(10)
                .level(2)
                .groupAchievement(groupAchievementEntity)
                .img("http://img")
                .build();

        AchievementEntity entity2 = AchievementEntity.builder()
                .typeOfAchievement("CREATE_FIVE_TEST")
                .action(TEST_CREATING)
                .threshold(7)
                .level(3)
                .groupAchievement(groupAchievementEntity)
                .img("http://img")
                .build();

        AchievementEntity entity3 = AchievementEntity.builder()
                .typeOfAchievement("CREATE_TEN_TEST")
                .action(TEST_CREATING)
                .threshold(5)
                .level(4)
                .groupAchievement(groupAchievementEntity)
                .img("http://img")
                .build();

        return Arrays.asList(entity1, entity2, entity3);
    }

    public static AchievementCreateDto generateAchievementCreateDto(){
        AchievementCreateDto createDto = new AchievementCreateDto();
        createDto.setImg("http://img");
        createDto.setLevel(3);
        createDto.setTypeOfAchievement("CREATE_TEN_TEST");
        createDto.setThreshold(10);
        createDto.setAction(TEST_CREATING);

        return createDto;
    }

    public static AchievementUpdateDto generateAchievementUpdateDto(Integer id, Integer threshold){
        AchievementUpdateDto updateDto = new AchievementUpdateDto();
        updateDto.setId(id);
        updateDto.setImg("http://img");
        updateDto.setLevel(3);
        updateDto.setTypeOfAchievement("CREATE_ONE_TEST");
        updateDto.setThreshold(threshold);
        updateDto.setAction(TEST_CREATING);

        return updateDto;
    }

    public static PlayerProgressEntity generationPlayerProgress(){
        PlayerProgressEntity entity = PlayerProgressEntity.builder()
                .action(TEST_PASSING)
                .counterAction(1)
                .build();

        return entity;
    }

    public static List<PlayerProgressEntity> generationListPlayerProgress(){
        PlayerProgressEntity entity1 = PlayerProgressEntity.builder()
                .action(TEST_PASSING)
                .counterAction(1)
                .build();

        PlayerProgressEntity entity2 = PlayerProgressEntity.builder()
                .action(TEST_CREATING)
                .counterAction(1)
                .build();

        PlayerProgressEntity entity3 = PlayerProgressEntity.builder()
                .action(TEST_PASSING)
                .counterAction(1)
                .build();

        return Arrays.asList(entity1, entity2, entity3);
    }

    public static PlayerCommittedActionDto generationPlayerCommittedActionDto(){
        PlayerCommittedActionDto committedActionDto = new PlayerCommittedActionDto();
        committedActionDto.setAction(TEST_PASSING);
        committedActionDto.setUserId((int) (Math.random() * 100));

        return committedActionDto;
    }

    public static PlayerEntity generatePlayer(){
        PlayerEntity entity = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(JUNIOR)
                .build();

        return entity;
    }

    public static List<PlayerEntity> generateListPlayers(){
        PlayerEntity entity1 = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(JUNIOR)
                .build();

        PlayerEntity entity2 = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(JUNIOR)
                .build();

        PlayerEntity entity3 = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(JUNIOR)
                .build();

        return Arrays.asList(entity1, entity2, entity3);
    }

    public static PlayerCreateDto generatePlayerCreateDto(){
        PlayerCreateDto createDto = new PlayerCreateDto();
        createDto.setUserId((int) (Math.random() * 100));

        return createDto;
    }

    public static GroupAchievementEntity generationGroupAchievement(){
        GroupAchievementEntity entity = GroupAchievementEntity.builder()
                .groupName("Road_Map")
                .backgroundImg("green")
                .build();

        return entity;
    }

    public static GroupAchievementCreateDto generateGroupAchievementCreateDto(){
        GroupAchievementCreateDto createDto = new GroupAchievementCreateDto();
        createDto.setGroupName("Road_map");
        createDto.setBackgroundImg("green");

        return createDto;
    }

    public static List<GroupAchievementEntity> generateListGroupAchievements(){
        GroupAchievementEntity entity1 = GroupAchievementEntity.builder()
                .groupName("Road_Map")
                .backgroundImg("green")
                .build();

        GroupAchievementEntity entity2 = GroupAchievementEntity.builder()
                .groupName("Java start")
                .backgroundImg("Red")
                .build();

        GroupAchievementEntity entity3 = GroupAchievementEntity.builder()
                .groupName("Road_Map_2")
                .backgroundImg("Blue")
                .build();

        return Arrays.asList(entity1, entity2, entity3);
    }

    public static GroupAchievementUpdateDto generateGroupAchievementUpdateDto(
            Integer id, String groupName, String backgroundImg){
        GroupAchievementUpdateDto updateDto = new GroupAchievementUpdateDto();
        updateDto.setId(id);
        updateDto.setGroupName(groupName);
        updateDto.setBackgroundImg(backgroundImg);
        return updateDto;
    }
}
