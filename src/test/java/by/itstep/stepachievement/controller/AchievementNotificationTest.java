package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.stepachievement.repository.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AchievementNotificationTest extends AbstractIntegrationTest {

    @Test
    void findAll_happyPath() throws Exception {
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        PlayerEntity foundPlayer = playerRepository.findByUserId(savePlayerProgressFullDto.getPlayerId());

        //when
        MvcResult result = mockMvc.perform(get("/achievementNotifications/users/{userId}"
                , foundPlayer.getUserId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<AchievementNotificationFullDto> found = objectMapper
                .readValue(bytes, new TypeReference<List<AchievementNotificationFullDto>>() {
                });

        //then
        assertNotNull(found);
        assertNotEquals(0, found.size());
    }

    @Test
    void findAll_exceptionPaths() throws Exception {
        // given
        Integer userId = 1000;

        //when
        MvcResult result = mockMvc.perform(get("/achievementNotifications/users/{userId}", userId))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void clearByNotificationId_happyPath() throws Exception {
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        List<AchievementNotificationFullDto> allAchievementByUserId = achievementNotificationService
                .findAllByUserId(savePlayerProgressFullDto.getPlayerId());

        Integer notificationId = allAchievementByUserId.get(allAchievementByUserId.size() - 1).getId();

        //when
        MvcResult result = mockMvc.perform(delete("/achievementNotifications/{id}",notificationId))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void clearByNotificationId_exceptionPaths() throws Exception {
        // given
        Integer notificationId = 1000;

        //when
        MvcResult result = mockMvc.perform(delete("/achievementNotifications/{id}",notificationId))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void clearByUserId_happyPath() throws Exception {
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        achievementNotificationService.findAllByUserId(savePlayerProgressFullDto.getPlayerId());

        PlayerEntity foundPlayer = playerRepository.findByUserId(savePlayerProgressFullDto.getPlayerId());

        //when
        MvcResult result = mockMvc.perform(delete("/achievementNotifications/users/{userId}"
                ,foundPlayer.getUserId()))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void clearByUserId_exceptionPaths() throws Exception {
        // given
        Integer playerId = 1000;

        //when
        MvcResult result = mockMvc.perform(delete("/achievementNotifications/users/{userId}", playerId))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
