package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.dto.achievement.AchievementUpdateDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.enums.Action;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.stepachievement.controller.GenerationUtils.*;
import static by.itstep.stepachievement.repository.EntityGenerationUtils.generationGroupAchievement;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AchievementControllerTest extends AbstractIntegrationTest {

    @Before
    private GroupAchievementEntity saveGroupAchievement(){
        return groupAchievementRepository.save(generationGroupAchievement());
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        AchievementEntity savedAchievement = achievementRepository.save(generateAchievement(saveGroupAchievement()));

        //when
        MvcResult result = mockMvc.perform(get("/achievements/{id}", savedAchievement.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        AchievementFullDto found = objectMapper.readValue(bytes, AchievementFullDto.class);

        //then
        assertNotNull(found);
        assertNotNull(found.getId());
        assertEquals(savedAchievement.getId(), found.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(get("/achievements/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<AchievementEntity> savedAchievements = achievementRepository
                .saveAll(generateListAchievement(saveGroupAchievement()));

        //when
        MvcResult result = mockMvc.perform(get("/achievements" ))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<AchievementPreviewDto> foundAchievements = objectMapper
                .readValue(bytes, new TypeReference<List<AchievementPreviewDto>>() {
                });

        //then
        assertNotNull(foundAchievements);
        assertEquals(savedAchievements.size(), foundAchievements.size());
    }

    @Test
    void findAllAction_happyPath() throws Exception {
        //given
        MvcResult result = mockMvc.perform(get("/achievements/action" ))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<Action> foundActions = objectMapper
                .readValue(bytes, new TypeReference<List<Action>>() {
                });

        //then
        assertNotNull(foundActions);

    }

    @Test
    void  create_happyPath() throws Exception {
        //given
        GroupAchievementEntity saveGroup = groupAchievementRepository.save(generationGroupAchievement());

        AchievementCreateDto createDto = generateAchievementCreateDto();
        createDto.setGroupId(saveGroup.getId());

        MvcResult result = mockMvc.perform(post("/achievements/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        //when
        byte[] bytes = result.getResponse().getContentAsByteArray();
        AchievementFullDto savedAchievement = objectMapper.readValue(bytes, AchievementFullDto.class);

        //then
        assertNotNull(savedAchievement);
        assertNotNull(savedAchievement.getGroupId());
    }

    @Test
    void  create_exceptionPaths() throws Exception {
        //given
        Integer groupId = 1000;
        AchievementCreateDto createDto = generateAchievementCreateDto();
        createDto.setGroupId(groupId);

        //when
        mockMvc.perform(post("/achievements/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isNotFound())
                .andReturn();

        //then
    }

    @Test
    void update_happyPath() throws Exception{
        //given
        Integer threshold = 1000;

        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity savedEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity savedAchievement = achievementRepository.save(generateAchievement(saveGroupAchievement()));

        AchievementUpdateDto updateDto = generateAchievementUpdateDto(savedAchievement.getId(), threshold);
        updateDto.setGroupId(savedEntity.getId());

        //when
        MvcResult result = mockMvc.perform(put("/achievements/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        AchievementFullDto updateAchievements = objectMapper.readValue(bytes, AchievementFullDto.class);

        //then
        assertNotNull(updateAchievements);
        assertNotNull(updateAchievements.getId());
        assertEquals(savedAchievement.getId(), updateAchievements.getId());
        assertNotEquals(savedAchievement.getThreshold(), updateAchievements.getThreshold());
    }

    @Test
    void update_exceptionPaths() throws Exception{
        //given
        Integer threshold = 1000;
        Integer groupId = 1000;
        AchievementEntity savedAchievement = achievementRepository.save(generateAchievement(saveGroupAchievement()));

        AchievementUpdateDto updateDto = generateAchievementUpdateDto(savedAchievement.getId(), threshold);
        updateDto.setGroupId(groupId);

        //when
        MvcResult result = mockMvc.perform(put("/achievements/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        AchievementEntity savedAchievement = achievementRepository.save(generateAchievement(saveGroupAchievement()));

        //when
        MvcResult result = mockMvc.perform(delete("/achievements/{id}", savedAchievement.getId()))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(delete("/achievements/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }
}
