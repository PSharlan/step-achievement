package by.itstep.stepachievement.controller;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.stepachievement.controller.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlayerControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        //given
        PlayerEntity savedPlayer = playerRepository.save(generatePlayer());

        //when
        MvcResult result = mockMvc.perform(get("/players/{id}", savedPlayer.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlayerFullDto found = objectMapper.readValue(bytes, PlayerFullDto.class);

        //then
        assertNotNull(found);
        assertNotNull(found.getId());
        assertEquals(savedPlayer.getId(), found.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(get("/players/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<PlayerEntity> savedPlayers = playerRepository.saveAll(generateListPlayers());

        //when
        MvcResult result = mockMvc.perform(get("/players" ))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<PlayerPreviewDto> foundPlayers = objectMapper
                .readValue(bytes, new TypeReference<List<PlayerPreviewDto>>() {
                });

        //then
        assertNotNull(foundPlayers);
        assertEquals(savedPlayers.size(), foundPlayers.size());
    }

    @Test
    void  create_happyPath() throws Exception {
        //given
        PlayerCreateDto createDto = generatePlayerCreateDto();

        MvcResult result = mockMvc.perform(post("/players/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        //when
        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlayerFullDto savedPlayer = objectMapper.readValue(bytes, PlayerFullDto.class);

        //then
        assertNotNull(savedPlayer);
    }

    @Test
    void create_exceptionPaths() throws Exception {
        //given
        PlayerCreateDto createDto = generatePlayerCreateDto();

        createDto.setUserId(null);

        //when
        mockMvc.perform(post("/players/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        PlayerEntity savedPlayer = playerRepository.save(generatePlayer());

        //when
        MvcResult result = mockMvc.perform(get("/players/{id}", savedPlayer.getId()))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when
        mockMvc.perform(delete("/players/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }
}
