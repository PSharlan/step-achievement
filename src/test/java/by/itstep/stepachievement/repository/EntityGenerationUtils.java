package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.entity.PlayerProgressEntity;

import java.util.Arrays;
import java.util.List;

import static by.itstep.stepachievement.entity.enums.Action.TEST_PASSING;
import static by.itstep.stepachievement.entity.enums.PlayerRank.*;

public class EntityGenerationUtils {

    public static AchievementEntity generateAchievement(GroupAchievementEntity groupAchievementEntity){
        AchievementEntity entity = AchievementEntity.builder()
                .typeOfAchievement("CREATE_ONE_TEST")
                .action(TEST_PASSING)
                .threshold(1)
                .groupAchievement(groupAchievementEntity)
                .level(1)
                .img("http://img")
                .build();

        return entity;
    }

    public static PlayerEntity generatePlayer(){
        PlayerEntity entity = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(JUNIOR)
                .build();

        return entity;
    }

    public static List<PlayerEntity> generatePlayerList(){
        PlayerEntity entity1 = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(JUNIOR)
                .build();

        PlayerEntity entity2 = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(MIDDLE)
                .build();

        PlayerEntity entity3 = PlayerEntity.builder()
                .userId((int) (Math.random() * 100))
                .playerRank(SENOR)
                .build();

        return Arrays.asList(entity1, entity2, entity3);
    }

    public static PlayerProgressEntity generationPlayerProgress(){
        PlayerProgressEntity entity = PlayerProgressEntity.builder()
                .action(TEST_PASSING)
                .counterAction(1)
                .build();

        return entity;
    }

    public static GroupAchievementEntity generationGroupAchievement(){
        GroupAchievementEntity entity = GroupAchievementEntity.builder()
                .groupName("Road_Map" + (int) (Math.random() * 100))
                .backgroundImg("green")
                .build();

        return entity;
    }

    public static PlayerCommittedActionDto generationPlayerCommittedActionDto(){
        PlayerCommittedActionDto committedActionDto = new PlayerCommittedActionDto();
        committedActionDto.setAction(TEST_PASSING);
        committedActionDto.setUserId((int) (Math.random() * 100));

        return committedActionDto;
    }
}
