package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.PlayerProgressEntity;

import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;
import java.util.Optional;

import static by.itstep.stepachievement.entity.enums.Action.*;
import static by.itstep.stepachievement.repository.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PlayerProgressTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath(){
        // given
        PlayerProgressEntity toSave = generationPlayerProgress();
        PlayerProgressEntity saved = playerProgressRepository.save(toSave);

        // when
        Optional<PlayerProgressEntity> found = playerProgressRepository.findById(saved.getId());

        // then
        assertTrue(found.isPresent());
        assertEquals(saved.getId(), found.get().getId());
    }

    @Test
    void create_happyPath(){
        // given
        PlayerProgressEntity toSave = generationPlayerProgress();

        // when
        PlayerProgressEntity saved = playerProgressRepository.save(toSave);

        // then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath(){
        // given
        PlayerProgressEntity toSave = generationPlayerProgress();
        PlayerProgressEntity saved = playerProgressRepository.save(toSave);
        Optional<PlayerProgressEntity> found = playerProgressRepository.findById(saved.getId());

        // when
        found.get().setAction(TEST_CREATING);
        playerProgressRepository.save(found.get());

        // then
        assertNotEquals(found.get().getAction(),saved.getAction());
        assertEquals(found.get().getId(),saved.getId());
    }

    @Test
    void delete_happyPath(){
        // given
        PlayerProgressEntity toSave = generationPlayerProgress();
        PlayerProgressEntity saved = playerProgressRepository.save(toSave);

        // when
        playerProgressRepository.delete(saved);

        // then
        assertTrue(playerProgressRepository.findById(saved.getId()).isEmpty());
    }

    @Test
    void findAll_happyPath(){
        // given
        PlayerProgressEntity toSave1 = generationPlayerProgress();
        PlayerProgressEntity toSave2 = generationPlayerProgress();
        playerProgressRepository.save(toSave1);
        playerProgressRepository.save(toSave2);

        // when
        List<PlayerProgressEntity> PlayerProgressEntityList = playerProgressRepository.findAll();

        // then
        assertEquals(2,PlayerProgressEntityList.size());
    }
}