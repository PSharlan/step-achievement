package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.*;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.stepachievement.repository.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class AchievementNotificationTest extends AbstractIntegrationTest {

    @Test
    void findAll_happyPath(){
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());
        PlayerEntity byUserId = playerRepository.findByUserId(savePlayerProgressFullDto.getPlayerId());

        //when
        List<AchievementNotificationEntity> allAchievementByUserId = achievementNotificationRepository
                .findAllByPlayerId(byUserId.getId());

        //then
        assertNotNull(allAchievementByUserId);
        assertNotEquals(0, allAchievementByUserId.size());
    }
}
