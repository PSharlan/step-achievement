package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static by.itstep.stepachievement.repository.EntityGenerationUtils.generationGroupAchievement;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GroupAchievementTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath(){
        // given
        GroupAchievementEntity toSave = generationGroupAchievement();
        GroupAchievementEntity saved = groupAchievementRepository.save(toSave);

        // when
        Optional<GroupAchievementEntity>  found = groupAchievementRepository.findById(saved.getId());

        // then
        assertTrue(found.isPresent());
        assertEquals(saved.getId(), found.get().getId());
    }

    @Test
    void findAll_happyPath(){
        // given
        GroupAchievementEntity toSave1 = generationGroupAchievement();
        GroupAchievementEntity toSave2 = generationGroupAchievement();
        groupAchievementRepository.save(toSave1);
        groupAchievementRepository.save(toSave2);

        // when
        List<GroupAchievementEntity> listEntity = groupAchievementRepository.findAll();

        //then
        assertEquals(2, listEntity.size());
    }

    @Test
    void create_happyPath(){
        // given
        GroupAchievementEntity toSave = generationGroupAchievement();

        // when
        GroupAchievementEntity saved = groupAchievementRepository.save(toSave);

        // then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath(){
        // given
        GroupAchievementEntity toSave = generationGroupAchievement();
        GroupAchievementEntity saved = groupAchievementRepository.save(toSave);
        Optional<GroupAchievementEntity>  foundEntity = groupAchievementRepository.findById(saved.getId());

        // when
        saved.setGroupName("Java start");
        saved.setBackgroundImg("Red");

        GroupAchievementEntity updateEntity = groupAchievementRepository.save(saved);

        // then
        assertNotEquals(foundEntity.get().getGroupName(), updateEntity.getGroupName());
        assertEquals("Red", updateEntity.getBackgroundImg());
        assertEquals(foundEntity.get().getId(), updateEntity.getId());
    }

    @Test
    void delete_happyPath(){
        // given
        GroupAchievementEntity toSave = generationGroupAchievement();
        GroupAchievementEntity saved = groupAchievementRepository.save(toSave);

        // when
        groupAchievementRepository.delete(saved);

        // then
        assertTrue(groupAchievementRepository.findById(saved.getId()).isEmpty());
    }
}
