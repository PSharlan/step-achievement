package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static by.itstep.stepachievement.repository.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class AchievementTest extends AbstractIntegrationTest {

    @Before
    private GroupAchievementEntity saveGroupAchievement(){
        return groupAchievementRepository.save(generationGroupAchievement());
    }

    @Test
    void findAll_happyPath(){
        // given
        AchievementEntity toSave1 = generateAchievement(saveGroupAchievement());
        AchievementEntity toSave2 = generateAchievement(saveGroupAchievement());

        achievementRepository.save(toSave1);
        achievementRepository.save(toSave2);

        // when
        List<AchievementEntity> listEntity = achievementRepository.findAll();

        //then
        assertEquals(2, listEntity.size());
    }

    @Test
    void findById_happyPath(){
        // given
        AchievementEntity toSave = generateAchievement(saveGroupAchievement());
        AchievementEntity saved = achievementRepository.save(toSave);

        // when
        Optional<AchievementEntity> found = achievementRepository.findById(saved.getId());

        // then
        assertTrue(found.isPresent());
        assertEquals(saved.getId(), found.get().getId());
    }

    @Test
    void create_happyPath(){
        // given
        AchievementEntity toSave = generateAchievement(saveGroupAchievement());

        // when
        AchievementEntity saved = achievementRepository.save(toSave);

        // then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath(){
        // given
        AchievementEntity toSave = generateAchievement(saveGroupAchievement());
        AchievementEntity saved = achievementRepository.save(toSave);
        Optional<AchievementEntity>  foundEntity = achievementRepository.findById(saved.getId());

        // when
        saved.setTypeOfAchievement("PASSING_FIVE_TEST");
        saved.setImg("testImg");
        AchievementEntity updateEntity = achievementRepository.save(saved);

        // then
        assertNotEquals(foundEntity.get().getImg(), updateEntity.getImg());
        assertEquals("testImg", updateEntity.getImg());
        assertEquals(foundEntity.get().getId(), updateEntity.getId());
    }

    @Test
    void delete_happyPath(){
        // given
        AchievementEntity toSave = generateAchievement(saveGroupAchievement());
        AchievementEntity saved = achievementRepository.save(toSave);

        // when
        achievementRepository.delete(saved);

        // then
        assertTrue(achievementRepository.findById(saved.getId()).isEmpty());
    }
}
