package by.itstep.stepachievement.repository;

import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.Optional;

import static by.itstep.stepachievement.entity.enums.PlayerRank.*;
import static by.itstep.stepachievement.repository.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath(){
        // given
        PlayerEntity toSave = generatePlayer();
        PlayerEntity saved = playerRepository.save(toSave);

        // when
        Optional<PlayerEntity> found = playerRepository.findById(saved.getId());

        // then
        assertNotNull(found);
        assertEquals(saved.getId(), found.get().getId());
    }

    @Test
    void create_happyPath(){
        // given
        PlayerEntity toSave = generatePlayer();

        // when
        PlayerEntity saved = playerRepository.save(toSave);

        // then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath(){
        // given
        PlayerEntity toSave = generatePlayer();
        PlayerEntity saved = playerRepository.save(toSave);
        Optional<PlayerEntity> found = playerRepository.findById(saved.getId());

        // when
        found.get().setPlayerRank(MIDDLE);
        playerRepository.save(found.get());

        // then
        assertNotEquals(found.get().getPlayerRank(),saved.getPlayerRank());
        assertEquals(found.get().getId(),saved.getId());
    }

    @Test
    void delete_happyPath(){
        // given
        PlayerEntity toSave = generatePlayer();
        PlayerEntity saved = playerRepository.save(toSave);

        // when
        playerRepository.delete(saved);

        // then
        assertTrue(playerRepository.findById(saved.getId()).isEmpty());
    }

    @Test
    void findAll_happyPath(){
        // given
        PlayerEntity toSave1 = generatePlayer();
        PlayerEntity toSave2 = generatePlayer();
        playerRepository.save(toSave1);
        playerRepository.save(toSave2);

        // when
        List<PlayerEntity> PlayerEntityList = playerRepository.findAll();

        // then
        assertEquals(2,PlayerEntityList.size());
    }
}
