package by.itstep.stepachievement.integration;

import by.itstep.stepachievement.StepAchievementApplication;
import by.itstep.stepachievement.integration.initalizer.MySqlInitializer;
import by.itstep.stepachievement.mapper.*;
import by.itstep.stepachievement.repository.*;
import by.itstep.stepachievement.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
@ContextConfiguration(initializers = MySqlInitializer.class,
        classes = StepAchievementApplication.class)
public abstract class AbstractIntegrationTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected PlayerService playerService;

    @Autowired
    protected PlayerProgressService playerProgressService;

    @Autowired
    protected AchievementService achievementService;

    @Autowired
    protected PlayerRepository playerRepository;

    @Autowired
    protected PlayerProgressRepository playerProgressRepository;

    @Autowired
    protected AchievementRepository achievementRepository;

    @Autowired
    protected AchievementMapper achievementMapper;

    @Autowired
    protected PlayerMapper playerMapper;

    @Autowired
    protected PlayerProgressMapper playerProgressMapper;

    @Autowired
    protected GroupAchievementMapper groupAchievementMapper;

    @Autowired
    protected GroupAchievementService groupAchievementService;

    @Autowired
    protected GroupAchievementRepository groupAchievementRepository;

    @Autowired
    protected AchievementNotificationRepository achievementNotificationRepository;

    @Autowired
    protected AchievementNotificationService achievementNotificationService;

    @Autowired
    protected AchievementNotificationMapper achievementNotificationMapper;

    @BeforeEach
    public void prepareDatabase(){
        achievementNotificationRepository.deleteAll();
        playerProgressRepository.deleteAll();
        playerRepository.deleteAll();
        achievementRepository.deleteAll();
        groupAchievementRepository.deleteAll();
    }
}
