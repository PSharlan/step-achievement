package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.entity.PlayerProgressEntity;

import java.util.Arrays;
import java.util.List;

import static by.itstep.stepachievement.entity.enums.Action.TEST_CREATING;
import static by.itstep.stepachievement.entity.enums.Action.TEST_PASSING;
import static by.itstep.stepachievement.entity.enums.PlayerRank.*;

public class GenerationUtils {
    public static AchievementEntity generateAchievement(){
        AchievementEntity entity = AchievementEntity.builder()
                .typeOfAchievement("CREATE_ONE_TEST")
                .action(TEST_CREATING)
                .level(2)
                .threshold(10)
                .img("http://img")
                .build();

        return entity;
    }

    public static AchievementCreateDto generateAchievementCreateDto(){
        AchievementCreateDto createDto = new AchievementCreateDto();
        createDto.setTypeOfAchievement("CREATE_ONE_TEST");
        createDto.setImg("http://img");

        return createDto;
    }

    public static PlayerCreateDto generatePlayerCreateDto(){
        PlayerCreateDto createDto = new PlayerCreateDto();
        createDto.setUserId((int)Math.random() * 100);

        return createDto;
    }

    public static PlayerEntity generatePlayer(){
        PlayerEntity entity = PlayerEntity.builder()
                .userId((int)Math.random() * 100)
                .playerRank(JUNIOR)
                .build();

        return entity;
    }

    public static List<PlayerEntity> generatePlayerList(){
        PlayerEntity entity1 = PlayerEntity.builder()
                .userId((int)Math.random() * 100)
                .playerRank(JUNIOR)
                .build();

        PlayerEntity entity2 = PlayerEntity.builder()
                .userId((int)Math.random() * 100)
                .playerRank(MIDDLE)
                .build();

        PlayerEntity entity3 = PlayerEntity.builder()
                .userId((int)Math.random() * 100)
                .playerRank(SENOR)
                .build();

        return Arrays.asList(entity1, entity2, entity3);
    }

    public static PlayerProgressEntity generationPlayerProgress(){
        PlayerProgressEntity entity = PlayerProgressEntity.builder()
                .player(generatePlayer())
                .action(TEST_PASSING)
                .counterAction(1)
                .build();

        return entity;
    }

    public static PlayerCommittedActionDto generationPlayerProgressCreateDto(){
        PlayerCommittedActionDto createDto = new PlayerCommittedActionDto();
        createDto.setUserId((int)Math.random() * 100);
        createDto.setAction(TEST_PASSING);

        return createDto;
    }

    public static GroupAchievementEntity generationGroupAchievement(){
        GroupAchievementEntity entity = GroupAchievementEntity.builder()
                .groupName("Road_Map")
                .backgroundImg("green")
                .build();

        return entity;
    }

    public static GroupAchievementCreateDto generateGroupAchievementCreateDto(){
       GroupAchievementCreateDto createDto = new GroupAchievementCreateDto();
       createDto.setGroupName("Road_map");
       createDto.setBackgroundImg("green");

       return createDto;
    }
}
