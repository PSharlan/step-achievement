package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static by.itstep.stepachievement.mapper.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GroupAchievementTest extends AbstractIntegrationTest {

    @Test
    public void mapEntityToFullDto_happyPath(){
        //given
        GroupAchievementEntity entity = generationGroupAchievement();

        //when
        GroupAchievementFullDto fullDto = groupAchievementMapper.mapToFullDto(entity);

        //then
        assertEquals(entity.getId(), fullDto.getId());
        assertEquals(entity.getGroupName(), fullDto.getGroupName());
        assertEquals(entity.getBackgroundImg(), fullDto.getBackgroundImg());
    }

    @Test
    public void mapCreateDtoToEntity_happyPath(){
        //given
        GroupAchievementCreateDto createDto = generateGroupAchievementCreateDto();

        //when
        GroupAchievementEntity entity = groupAchievementMapper.mapToEntity(createDto);

        //then
        assertNotNull(entity.getGroupName());
        assertNotNull(entity.getBackgroundImg());
        assertEquals(createDto.getBackgroundImg(), entity.getBackgroundImg());
    }
}
