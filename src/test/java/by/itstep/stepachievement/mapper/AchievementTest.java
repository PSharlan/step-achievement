package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static by.itstep.stepachievement.mapper.GenerationUtils.generateAchievement;
import static by.itstep.stepachievement.mapper.GenerationUtils.generateAchievementCreateDto;
import static org.junit.jupiter.api.Assertions.*;

public class AchievementTest extends AbstractIntegrationTest {

    @Test
    public void mapEntityToFullDto_happyPath(){
        //given
        AchievementEntity entity = generateAchievement();

        //when
        AchievementFullDto fullDto = achievementMapper.mapToFullDto(entity);

        //then
        assertEquals(entity.getId(), fullDto.getId());
        assertEquals(entity.getImg(), fullDto.getImg());
        assertEquals(entity.getTypeOfAchievement(), fullDto.getTypeOfAchievement());
    }

    @Test
    public void mapCreateDtoToEntity_happyPath(){
        //given
        AchievementCreateDto createDto = generateAchievementCreateDto();

        //when
        AchievementEntity entity = achievementMapper.mapToEntity(createDto);

        //then
        assertNotNull(entity.getImg());
        assertNotNull(entity.getTypeOfAchievement());
        assertEquals(createDto.getImg(), entity.getImg());
        assertEquals(createDto.getTypeOfAchievement(), entity.getTypeOfAchievement());
    }

    @Test
    void mapEntityToPreviewDto_happyPath(){
        //given
        AchievementEntity entity = generateAchievement();

        //when
        AchievementPreviewDto previewDto = achievementMapper.mapToPreviewDto(entity);

        //then
        assertNotNull(previewDto.getImg());
        assertNotNull(previewDto.getTypeOfAchievement());
        assertEquals(entity.getImg(), previewDto.getImg());
        assertEquals(entity.getTypeOfAchievement(), previewDto.getTypeOfAchievement());
    }
}
