package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;
import by.itstep.stepachievement.entity.AchievementNotificationEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static by.itstep.stepachievement.mapper.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AchievementNotificationTest extends AbstractIntegrationTest {

    @Test
    public void mapToFullDto(){
        //give
        AchievementNotificationEntity entity = AchievementNotificationEntity.builder()
                .player(generatePlayer())
                .achievement(generateAchievement())
                .build();

        //when
        AchievementNotificationFullDto fullDto = achievementNotificationMapper
                .mapToFullDto(entity);

        //then
        assertEquals(entity.getId(), fullDto.getId());
        assertEquals(entity.getAchievement().getAction(), fullDto.getAchievementPreviewDto().getAction());
        assertEquals(entity.getPlayer().getPlayerRank(), fullDto.getPlayerPreviewDto().getPlayerRank());
    }
}
