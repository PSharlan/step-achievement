package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static by.itstep.stepachievement.mapper.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest extends AbstractIntegrationTest {

    @Test
    public void mapEntityToFullDto_happyPath(){
        //given
        PlayerEntity entity = generatePlayer();

        //when
        PlayerFullDto fullDto = playerMapper.mapToFullDto(entity);

        //then
        assertNotNull(fullDto.getUserId());
        assertNotNull(fullDto.getPlayerRank());
        assertEquals(entity.getUserId(), fullDto.getUserId());
        assertEquals(entity.getPlayerRank(), fullDto.getPlayerRank());
    }

    @Test
    public void mapCreateDtoToEntity_happyPath(){
        //given
        PlayerCreateDto createDto = generatePlayerCreateDto();

        //when
        PlayerEntity entity = playerMapper.mapToEntity(createDto);

        //then
        assertNotNull(entity.getUserId());
        assertEquals(createDto.getUserId(), entity.getUserId());
    }

    @Test
    void mapEntityToPreviewDto_happyPath(){
        //given
        PlayerEntity entity = generatePlayer();

        //when
        PlayerPreviewDto previewDto = playerMapper.mapToPreviewDto(entity);

        //then
        assertNotNull(previewDto.getUserId());
        assertNotNull(previewDto.getPlayerRank());
        assertEquals(entity.getUserId(), previewDto.getUserId());
        assertEquals(entity.getPlayerRank(), previewDto.getPlayerRank());
    }
}
