package by.itstep.stepachievement.mapper;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.PlayerProgressEntity;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static by.itstep.stepachievement.mapper.GenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerProgressTest extends AbstractIntegrationTest {

    @Test
    public void mapEntityToFullDto_happyPath(){
        //given
        PlayerProgressEntity entity = generationPlayerProgress();

        //when
        PlayerProgressFullDto fullDto = playerProgressMapper.mapToFullDto(entity);

        //then
        assertNotNull(fullDto.getAction());
        assertNotNull(fullDto.getCounterAction());
        assertEquals(entity.getAction(), fullDto.getAction());
        assertEquals(entity.getCounterAction(), fullDto.getCounterAction());
    }

    @Test
    public void mapCreateDtoToEntity_happyPath(){
        //given
        PlayerCommittedActionDto createDto = generationPlayerProgressCreateDto();

        //when
        PlayerProgressEntity entity = playerProgressMapper.mapToEntity(createDto);

        //then
        assertNotNull(entity.getAction());
    }
}
