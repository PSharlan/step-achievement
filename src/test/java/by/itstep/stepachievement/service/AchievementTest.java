package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.achievement.AchievementCreateDto;
import by.itstep.stepachievement.dto.achievement.AchievementFullDto;
import by.itstep.stepachievement.dto.achievement.AchievementPreviewDto;
import by.itstep.stepachievement.dto.achievement.AchievementUpdateDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.enums.Action;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.stepachievement.repository.EntityGenerationUtils.generationGroupAchievement;
import static by.itstep.stepachievement.service.ServiceGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class AchievementTest extends AbstractIntegrationTest {

    @Before
    private GroupAchievementEntity saveGroupAchievement(){
        return groupAchievementRepository.save(generationGroupAchievement());
    }

    @Test
    void findById_happyPath(){
        //given
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository
                .save(generationGroupAchievement());

        AchievementEntity saved = generateAchievement();
        saved.setGroupAchievement(saveGroupAchievementEntity);

        AchievementEntity saveAchievementEntity = achievementRepository.save(saved);

        //when
        AchievementFullDto foundAchievement = achievementService.findById(saveAchievementEntity.getId());

        //then
        assertNotNull(foundAchievement);
        assertEquals(foundAchievement.getId(), saveAchievementEntity.getId());
        assertEquals(foundAchievement.getTypeOfAchievement(), saveAchievementEntity.getTypeOfAchievement());
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 1000;

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> achievementService.findById(notExistingId));
    }

    @Test
    void findAll_happyPath(){
        //given
        List<AchievementEntity> resultSaved = achievementRepository
                .saveAll(generateListAchievements(saveGroupAchievement()));

        //when
        List<AchievementPreviewDto> foundListAchievements = achievementService.findAll();

        //then
        assertNotNull(foundListAchievements);
        assertNotNull(foundListAchievements.get(1).getImg());
        assertEquals(foundListAchievements.size(), resultSaved.size());
    }

    @Test
    void create_happyPath(){
        //given
        AchievementCreateDto resultToSave = generateAchievementCreateDto();

        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievement = groupAchievementRepository.save(groupAchievementEntity);

        //when
        resultToSave.setGroupId(saveGroupAchievement.getId());
        AchievementFullDto savedAchievement = achievementService.create(resultToSave);
        AchievementFullDto foundResult = achievementService.findById(savedAchievement.getId());

        //then
        assertNotNull(foundResult);
        assertNotNull(foundResult.getId());
        assertNotNull(foundResult.getGroupId());
        assertEquals(savedAchievement.getId(), foundResult.getId());
        assertEquals(savedAchievement.getTypeOfAchievement(), foundResult.getTypeOfAchievement());
    }

    @Test
    void create_exceptionPath(){
        //given
        Integer groupId = 1000;
        AchievementCreateDto resultToSave = generateAchievementCreateDto();

        //when
        resultToSave.setGroupId(groupId);

        //then
        assertThrows(EntityIsNotFoundException.class, () -> achievementService.create(resultToSave));
    }

    @Test
    void update_happyPath(){
        //given
        AchievementCreateDto resultToSave = generateAchievementCreateDto();
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();

        GroupAchievementEntity saveGroupAchievement = groupAchievementRepository.save(groupAchievementEntity);
        resultToSave.setGroupId(groupAchievementEntity.getId());

        AchievementFullDto savedAchievement = achievementService.create(resultToSave);
        AchievementUpdateDto updateDto = generateAchievementUpdateDto(savedAchievement.getId(), 100);

        updateDto.setGroupId(saveGroupAchievement.getId());

        //when
        AchievementFullDto resultUpdate = achievementService.update(updateDto);

        //then
        assertNotNull(resultUpdate);
        assertNotNull(resultUpdate.getGroupId());
        assertEquals(resultUpdate.getId(), updateDto.getId());
        assertNotEquals(resultUpdate.getThreshold(), savedAchievement.getThreshold());
    }

    @Test
    void delete_happyPath(){
        //given
        AchievementCreateDto resultToSave = generateAchievementCreateDto();

        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievement = groupAchievementRepository.save(groupAchievementEntity);

        //when
        resultToSave.setGroupId(saveGroupAchievement.getId());
        AchievementFullDto savedAchievement = achievementService.create(resultToSave);

        //when
        achievementService.delete(savedAchievement.getId());

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> achievementService.findById(savedAchievement.getId()));
    }

    @Test
    void findAllAction_happyPath(){
        //given
        List<Action> allActions = achievementService.findAllAction();

        //then
        assertNotNull(allActions);
    }
}
