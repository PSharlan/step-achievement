package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.achievementNotification.AchievementNotificationFullDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.stepachievement.repository.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class AchievementNotificationTest extends AbstractIntegrationTest {

    @Test
    void findAll_happyPath(){
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        //when
        List<AchievementNotificationFullDto> allAchievementByUserId = achievementNotificationService
                .findAllByUserId(savePlayerProgressFullDto.getPlayerId());

        //then
        assertNotNull(allAchievementByUserId);
        assertNotEquals(0, allAchievementByUserId.size());
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 1000;

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> achievementNotificationService.findAllByUserId(notExistingId));
    }

    @Test
    void clearById_happyPath(){
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        List<AchievementNotificationFullDto> allAchievementByUserId = achievementNotificationService
                .findAllByUserId(savePlayerProgressFullDto.getPlayerId());

        //when
        Integer notificationId = allAchievementByUserId.get(allAchievementByUserId.size() - 1).getId();
        achievementNotificationService.clearById(notificationId);

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> achievementNotificationService.clearById(notificationId));
    }

    @Test
    void clearById_exceptionPath(){
        // given
        Integer notExistingId = 1000;

        //when
        assertThrows(EntityIsNotFoundException.class,
                () -> achievementNotificationService.clearById(notExistingId));

        //then
    }

    @Test
    void clearByUserId_happyPath(){
        // given
        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity achievementEntity = generateAchievement(saveGroupAchievementEntity);
        achievementRepository.save(achievementEntity);

        PlayerProgressFullDto savePlayerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        //when
        PlayerEntity byUserId = playerRepository.findByUserId(savePlayerProgressFullDto.getPlayerId());
        achievementNotificationService.clearByUserId(byUserId.getUserId());

        List<AchievementNotificationFullDto> allAchievementByUserId = achievementNotificationService
                .findAllByUserId(savePlayerProgressFullDto.getPlayerId());

        //then
        assertEquals(new ArrayList<AchievementNotificationFullDto>(), allAchievementByUserId);
    }
}
