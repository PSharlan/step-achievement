package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.player.PlayerCreateDto;
import by.itstep.stepachievement.dto.player.PlayerFullDto;
import by.itstep.stepachievement.dto.player.PlayerPreviewDto;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.exception.InvalidEntityException;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.stepachievement.service.ServiceGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath(){
        //give
        PlayerEntity saved = playerRepository.save(generatePlayer());

        //when
        PlayerFullDto foundPlayer = playerService.findById(saved.getId());

        //then
        assertNotNull(foundPlayer);
        assertNotNull(foundPlayer.getUserId());
        assertEquals(foundPlayer.getId(), saved.getId());
    }

    @Test
    void findById_whenNotFound() {
        //given
        Integer notExistingId = 1000;

        //then
        assertThrows(EntityIsNotFoundException.class,
                () ->playerService.findById(notExistingId));
    }

    @Test
    void findAll_happyPath(){
        //given
        List<PlayerEntity> savedList = playerRepository.saveAll(generateListPlayer());

        //when
        List<PlayerPreviewDto> foundListPlayer = playerService.findAll();

        //then
        assertNotNull(foundListPlayer);
        assertNotNull(foundListPlayer.get(2).getUserId());
        assertEquals(foundListPlayer.size(), savedList.size());
    }

    @Test
    void create_happyPath(){
        //given
        PlayerCreateDto playerCreateDto = generatePlayerCreateDto();

        //when
        PlayerFullDto savedResult = playerService.create(playerCreateDto);
        PlayerFullDto foundResult = playerService.findById(savedResult.getId());

        //then
        assertNotNull(foundResult);
        assertNotNull(foundResult.getId());
        assertNotNull(foundResult.getUserId());
        assertEquals(foundResult.getUserId(), savedResult.getUserId());
    }

    @Test
    void create_exceptionPath() {
        //given
        PlayerFullDto playerFullDto = playerService.create(generatePlayerCreateDto());

        //then
        PlayerCreateDto playerCreateDto = generatePlayerCreateDto();
        playerCreateDto.setUserId(playerFullDto.getUserId());

        //then
        assertThrows(InvalidEntityException.class,
                () -> playerService.create(playerCreateDto));
    }

    @Test
    void delete_happyPath(){
        //given
        PlayerFullDto resultToSave = playerService.create(generatePlayerCreateDto());

        //when
        playerService.delete(resultToSave.getId());

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> playerService.findById(resultToSave.getId()));
    }
}
