package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.playerProgress.PlayerCommittedActionDto;
import by.itstep.stepachievement.dto.playerProgress.PlayerProgressFullDto;
import by.itstep.stepachievement.entity.AchievementEntity;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.entity.PlayerEntity;
import by.itstep.stepachievement.entity.PlayerProgressEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.stepachievement.entity.enums.Action.TEST_CREATING;
import static by.itstep.stepachievement.service.ServiceGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerProgressTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath(){
        //given
        PlayerProgressFullDto fullDto = playerProgressService.processAction(generationPlayerCommittedActionDto());

        //when
        PlayerProgressFullDto foundPlayerProgress = playerProgressService
                .findById(fullDto.getId());

        //then
        assertNotNull(foundPlayerProgress);
        assertNotNull(foundPlayerProgress.getId());
        assertNotNull(foundPlayerProgress.getPlayerId());
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 1000;

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> playerProgressService.findById(notExistingId));
    }

    @Test
    void findAll_happyPath(){
        //given
        List<PlayerProgressEntity> resultSaved = playerProgressRepository
                .saveAll(generationListPlayerProgress());

        //when
        List<PlayerProgressFullDto> resultFound = playerProgressService
                .findAll();

        //then
        assertNotNull(resultFound);
        assertNotNull(resultFound.get(2));
        assertEquals(resultFound.size(), resultSaved.size());
    }

    @Test
    void create_happyPath(){
        //given
        PlayerCommittedActionDto committedActionDto = generationPlayerCommittedActionDto();

        //when
        PlayerProgressFullDto savedPlayerProgress = playerProgressService
                .processAction(committedActionDto);
        PlayerProgressFullDto foundResult = playerProgressService
                .findById(savedPlayerProgress.getId());

        //then
        assertNotNull(foundResult);
        assertNotNull(foundResult.getCounterAction());
    }

    @Test
    void updateCounterAction_happyPath(){
        //given
        PlayerCommittedActionDto committedActionDto = generationPlayerCommittedActionDto();

        //when
        PlayerProgressFullDto savedPlayerProgress = playerProgressService
                .processAction(committedActionDto);
        PlayerProgressFullDto updatePlayerProgress = playerProgressService
                .processAction(committedActionDto);

        //then
        assertNotEquals(updatePlayerProgress.getCounterAction(), savedPlayerProgress.getCounterAction());
    }

    @Test
    void giveAchievement(){
        //given
        Integer userId = 1;

        GroupAchievementEntity groupAchievementEntity = generationGroupAchievement();
        GroupAchievementEntity saveGroupAchievementEntity = groupAchievementRepository.save(groupAchievementEntity);

        AchievementEntity savedAchievement = AchievementEntity.builder()
                .typeOfAchievement("TEST_ONE_CREATE")
                .action(TEST_CREATING)
                .threshold(1)
                .level(3)
                .groupAchievement(saveGroupAchievementEntity)
                .img("//html.good")
                .build();
        achievementRepository.save(savedAchievement);

        PlayerCommittedActionDto savedCommittedActionDto = new PlayerCommittedActionDto();
        savedCommittedActionDto.setAction(savedAchievement.getAction());
        savedCommittedActionDto.setUserId(userId);
        PlayerProgressFullDto playerProgressFullDto = playerProgressService.processAction(savedCommittedActionDto);

        //when
        PlayerEntity foundPlayerEntity = playerRepository.findByUserId(playerProgressFullDto.getPlayerId());

        //then
        assertNotNull(foundPlayerEntity);
        assertNotNull(foundPlayerEntity.getAchievements());
        assertNotNull(foundPlayerEntity.getAchievements().get(0).getTypeOfAchievement());
    }

    @Test
    void  delete_happyPath(){
        //given
        PlayerProgressFullDto playerProgressFullDto = playerProgressService
                .processAction(generationPlayerCommittedActionDto());

        //when
        playerProgressService.delete(playerProgressFullDto.getId());

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> achievementService.findById(playerProgressFullDto.getId()));
    }
}
