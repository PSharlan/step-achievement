package by.itstep.stepachievement.service;

import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementCreateDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementFullDto;
import by.itstep.stepachievement.dto.groupAchievement.GroupAchievementUpdateDto;
import by.itstep.stepachievement.entity.GroupAchievementEntity;
import by.itstep.stepachievement.exception.EntityIsNotFoundException;
import by.itstep.stepachievement.exception.InvalidEntityException;
import by.itstep.stepachievement.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.stepachievement.service.ServiceGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class GroupAchievementTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath(){
        //given
        GroupAchievementEntity saved = groupAchievementRepository.save(generationGroupAchievement());

        //when
        GroupAchievementFullDto foundGroupAchievement = groupAchievementService.findById(saved.getId());

        //then
        assertNotNull(foundGroupAchievement);
        assertEquals(foundGroupAchievement.getId(), saved.getId());
        assertEquals(foundGroupAchievement.getBackgroundImg(), saved.getBackgroundImg());
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 1000;

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> groupAchievementService.findById(notExistingId));
    }

    @Test
    void findAll_happyPath(){
        //given
        List<GroupAchievementEntity> resultSaved = groupAchievementRepository.saveAll(generateListGroupAchievements());

        //when
        List<GroupAchievementFullDto> foundListGroupAchievements = groupAchievementService.findAll();

        //then
        assertNotNull(foundListGroupAchievements);
        assertNotNull(foundListGroupAchievements.get(1).getGroupName());
        assertEquals(foundListGroupAchievements.size(), resultSaved.size());
    }

    @Test
    void create_happyPath(){
        //given
        GroupAchievementCreateDto resultToSave = generateGroupAchievementCreateDto();

        //when
        GroupAchievementFullDto savedGroupAchievement = groupAchievementService.create(resultToSave);
        GroupAchievementFullDto foundResult = groupAchievementService.findById(savedGroupAchievement.getId());

        //then
        assertNotNull(foundResult);
        assertNotNull(foundResult.getId());
        assertEquals(savedGroupAchievement.getId(), foundResult.getId());
        assertEquals(savedGroupAchievement.getBackgroundImg(), foundResult.getBackgroundImg());
    }

    @Test
    void create_exceptionPath(){
        //given
        GroupAchievementCreateDto resultToSave = generateGroupAchievementCreateDto();
        groupAchievementService.create(resultToSave);

        //when
        GroupAchievementCreateDto resultToSave2 = generateGroupAchievementCreateDto();

        //then
        assertThrows(InvalidEntityException.class, () -> groupAchievementService.create(resultToSave2));
    }

    @Test
    void update_happyPath(){
        //given
        GroupAchievementCreateDto resultToSave = generateGroupAchievementCreateDto();

        GroupAchievementFullDto savedGroupAchievement = groupAchievementService.create(resultToSave);

        GroupAchievementUpdateDto updateDto = new GroupAchievementUpdateDto();
        updateDto.setId(savedGroupAchievement.getId());
        updateDto.setGroupName("Hello Java");
        updateDto.setBackgroundImg("Black");

        //when
        GroupAchievementFullDto resultUpdate = groupAchievementService.update(updateDto);

        //then
        assertNotNull(resultUpdate);
        assertEquals(resultUpdate.getId(), updateDto.getId());
        assertNotEquals(resultUpdate.getBackgroundImg(), savedGroupAchievement.getBackgroundImg());
    }
}
